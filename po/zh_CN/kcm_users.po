msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-11 01:00+0000\n"
"PO-Revision-Date: 2023-01-02 07:12\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/plasma-workspace/kcm_users.pot\n"
"X-Crowdin-File-ID: 25727\n"

#: package/contents/ui/ChangePassword.qml:27
#: package/contents/ui/UserDetailsPage.qml:156
#, kde-format
msgid "Change Password"
msgstr "修改密码"

#: package/contents/ui/ChangePassword.qml:33
#, kde-format
msgid "Set Password"
msgstr "设置密码"

#: package/contents/ui/ChangePassword.qml:56
#, kde-format
msgid "Password"
msgstr "密码"

#: package/contents/ui/ChangePassword.qml:71
#, kde-format
msgid "Confirm password"
msgstr "确认密码"

#: package/contents/ui/ChangePassword.qml:90
#: package/contents/ui/CreateUser.qml:65
#, kde-format
msgid "Passwords must match"
msgstr "两次输入的密码必须一致。"

#: package/contents/ui/ChangeWalletPassword.qml:17
#, kde-format
msgid "Change Wallet Password?"
msgstr "更改密码库密码？"

#: package/contents/ui/ChangeWalletPassword.qml:26
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Now that you have changed your login password, you may also want to change "
"the password on your default KWallet to match it."
msgstr ""
"您刚刚修改了登录密码，您可能想要同时更改 KWallet 密码库的密码，以避免每次登录"
"都要手动输入两次密码。"

#: package/contents/ui/ChangeWalletPassword.qml:31
#, kde-format
msgid "What is KWallet?"
msgstr "KWallet 密码库是什么？"

#: package/contents/ui/ChangeWalletPassword.qml:41
#, kde-format
msgid ""
"KWallet is a password manager that stores your passwords for wireless "
"networks and other encrypted resources. It is locked with its own password "
"which differs from your login password. If the two passwords match, it can "
"be unlocked at login automatically so you don't have to enter the KWallet "
"password yourself."
msgstr ""
"KWallet 密码库是一款密码管理器，用于存储您的无线网络密码和其他加密资源。它使"
"用独立于您登录密码的密码进行锁定。如果登录密码和密码库的密码相同，您可以设置"
"在登录时自动解锁密码库，免去再次输入密码的麻烦。"

#: package/contents/ui/ChangeWalletPassword.qml:57
#, kde-format
msgid "Change Wallet Password"
msgstr "更改密码库密码"

#: package/contents/ui/ChangeWalletPassword.qml:66
#, kde-format
msgid "Leave Unchanged"
msgstr "保持不变"

#: package/contents/ui/CreateUser.qml:16
#, kde-format
msgid "Create User"
msgstr "创建用户"

#: package/contents/ui/CreateUser.qml:30
#: package/contents/ui/UserDetailsPage.qml:124
#, kde-format
msgid "Name:"
msgstr "名称："

#: package/contents/ui/CreateUser.qml:34
#: package/contents/ui/UserDetailsPage.qml:131
#, kde-format
msgid "Username:"
msgstr "用户名："

#: package/contents/ui/CreateUser.qml:44
#: package/contents/ui/UserDetailsPage.qml:139
#, kde-format
msgid "Standard"
msgstr "标准"

#: package/contents/ui/CreateUser.qml:45
#: package/contents/ui/UserDetailsPage.qml:140
#, kde-format
msgid "Administrator"
msgstr "管理员"

#: package/contents/ui/CreateUser.qml:48
#: package/contents/ui/UserDetailsPage.qml:143
#, kde-format
msgid "Account type:"
msgstr "账号类型："

#: package/contents/ui/CreateUser.qml:53
#, kde-format
msgid "Password:"
msgstr "密码："

#: package/contents/ui/CreateUser.qml:58
#, kde-format
msgid "Confirm password:"
msgstr "确认密码："

#: package/contents/ui/CreateUser.qml:72
#, kde-format
msgid "Create"
msgstr "创建"

#: package/contents/ui/FingerprintDialog.qml:58
#, kde-format
msgid "Configure Fingerprints"
msgstr "配置指纹"

#: package/contents/ui/FingerprintDialog.qml:68
#, kde-format
msgctxt "@action:button 'all' refers to fingerprints"
msgid "Clear All"
msgstr "全部清除"

#: package/contents/ui/FingerprintDialog.qml:75
#, kde-format
msgid "Add"
msgstr "添加"

#: package/contents/ui/FingerprintDialog.qml:84
#, kde-format
msgid "Cancel"
msgstr "取消"

#: package/contents/ui/FingerprintDialog.qml:92
#, kde-format
msgid "Done"
msgstr "完成"

#: package/contents/ui/FingerprintDialog.qml:116
#, kde-format
msgid "Enrolling Fingerprint"
msgstr "正在录入指纹"

#: package/contents/ui/FingerprintDialog.qml:125
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly press your right index finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:127
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly press your right middle finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:129
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly press your right ring finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:131
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly press your right little finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:133
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid "Please repeatedly press your right thumb on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:135
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly press your left index finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:137
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly press your left middle finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:139
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly press your left ring finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:141
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly press your left little finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:143
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid "Please repeatedly press your left thumb on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:147
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly swipe your right index finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:149
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly swipe your right middle finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:151
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly swipe your right ring finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:153
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly swipe your right little finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:155
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid "Please repeatedly swipe your right thumb on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:157
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly swipe your left index finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:159
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly swipe your left middle finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:161
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly swipe your left ring finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:163
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid ""
"Please repeatedly swipe your left little finger on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:165
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
#| msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgid "Please repeatedly swipe your left thumb on the fingerprint sensor."
msgstr "请在指纹传感器上重复%1您的%2。"

#: package/contents/ui/FingerprintDialog.qml:179
#, kde-format
msgid "Finger Enrolled"
msgstr "指纹已录入"

#: package/contents/ui/FingerprintDialog.qml:209
#, kde-format
msgid "Pick a finger to enroll"
msgstr "选择要录入指纹的手指"

#: package/contents/ui/FingerprintDialog.qml:326
#, kde-format
msgid "Re-enroll finger"
msgstr "重新录入手指指纹"

#: package/contents/ui/FingerprintDialog.qml:333
#, kde-format
msgid "Delete fingerprint"
msgstr "删除指纹"

#: package/contents/ui/FingerprintDialog.qml:342
#, kde-format
msgid "No fingerprints added"
msgstr "未添加指纹"

#: package/contents/ui/main.qml:22
#, kde-format
msgid "Manage Users"
msgstr "管理用户"

#: package/contents/ui/main.qml:123
#, kde-format
msgid "Add New User"
msgstr "添加新用户"

#: package/contents/ui/PicturesSheet.qml:19
#, fuzzy, kde-format
#| msgid "Change Avatar"
msgctxt "@title"
msgid "Change Avatar"
msgstr "更换头像"

#: package/contents/ui/PicturesSheet.qml:22
#, fuzzy, kde-format
#| msgid "It's Nothing"
msgctxt "@item:intable"
msgid "It's Nothing"
msgstr "啥都不是"

#: package/contents/ui/PicturesSheet.qml:23
#, fuzzy, kde-format
#| msgid "Feisty Flamingo"
msgctxt "@item:intable"
msgid "Feisty Flamingo"
msgstr "火烈鸟"

#: package/contents/ui/PicturesSheet.qml:24
#, fuzzy, kde-format
#| msgid "Dragon's Fruit"
msgctxt "@item:intable"
msgid "Dragon's Fruit"
msgstr "火龙果"

#: package/contents/ui/PicturesSheet.qml:25
#, fuzzy, kde-format
#| msgid "Sweet Potato"
msgctxt "@item:intable"
msgid "Sweet Potato"
msgstr "番薯"

#: package/contents/ui/PicturesSheet.qml:26
#, fuzzy, kde-format
#| msgid "Ambient Amber"
msgctxt "@item:intable"
msgid "Ambient Amber"
msgstr "琥珀"

#: package/contents/ui/PicturesSheet.qml:27
#, fuzzy, kde-format
#| msgid "Sparkle Sunbeam"
msgctxt "@item:intable"
msgid "Sparkle Sunbeam"
msgstr "阳光"

#: package/contents/ui/PicturesSheet.qml:28
#, fuzzy, kde-format
#| msgid "Lemon-Lime"
msgctxt "@item:intable"
msgid "Lemon-Lime"
msgstr "柠檬绿"

#: package/contents/ui/PicturesSheet.qml:29
#, fuzzy, kde-format
#| msgid "Verdant Charm"
msgctxt "@item:intable"
msgid "Verdant Charm"
msgstr "青葱"

#: package/contents/ui/PicturesSheet.qml:30
#, fuzzy, kde-format
#| msgid "Mellow Meadow"
msgctxt "@item:intable"
msgid "Mellow Meadow"
msgstr "草地"

#: package/contents/ui/PicturesSheet.qml:31
#, fuzzy, kde-format
#| msgid "Tepid Teal"
msgctxt "@item:intable"
msgid "Tepid Teal"
msgstr "孔雀绿"

#: package/contents/ui/PicturesSheet.qml:32
#, fuzzy, kde-format
#| msgid "Plasma Blue"
msgctxt "@item:intable"
msgid "Plasma Blue"
msgstr "Plasma 蓝"

#: package/contents/ui/PicturesSheet.qml:33
#, fuzzy, kde-format
#| msgid "Pon Purple"
msgctxt "@item:intable"
msgid "Pon Purple"
msgstr "暖紫"

#: package/contents/ui/PicturesSheet.qml:34
#, fuzzy, kde-format
#| msgid "Bajo Purple"
msgctxt "@item:intable"
msgid "Bajo Purple"
msgstr "冷紫"

#: package/contents/ui/PicturesSheet.qml:35
#, fuzzy, kde-format
#| msgid "Burnt Charcoal"
msgctxt "@item:intable"
msgid "Burnt Charcoal"
msgstr "焦炭"

#: package/contents/ui/PicturesSheet.qml:36
#, fuzzy, kde-format
#| msgid "Paper Perfection"
msgctxt "@item:intable"
msgid "Paper Perfection"
msgstr "白纸"

#: package/contents/ui/PicturesSheet.qml:37
#, fuzzy, kde-format
#| msgid "Cafétera Brown"
msgctxt "@item:intable"
msgid "Cafétera Brown"
msgstr "咖啡"

#: package/contents/ui/PicturesSheet.qml:38
#, fuzzy, kde-format
#| msgid "Rich Hardwood"
msgctxt "@item:intable"
msgid "Rich Hardwood"
msgstr "木材"

#: package/contents/ui/PicturesSheet.qml:72
#, fuzzy, kde-format
#| msgid "Choose a picture"
msgctxt "@title"
msgid "Choose a picture"
msgstr "选择图片"

#: package/contents/ui/PicturesSheet.qml:104
#, fuzzy, kde-format
#| msgid "Choose File…"
msgctxt "@action:button"
msgid "Choose File…"
msgstr "选择文件…"

#: package/contents/ui/UserDetailsPage.qml:107
#, kde-format
msgid "Change avatar"
msgstr "更改头像"

#: package/contents/ui/UserDetailsPage.qml:152
#, kde-format
msgid "Email address:"
msgstr "电子邮件地址："

#: package/contents/ui/UserDetailsPage.qml:176
#, kde-format
msgid "Delete files"
msgstr "删除文件"

#: package/contents/ui/UserDetailsPage.qml:183
#, kde-format
msgid "Keep files"
msgstr "保留文件"

#: package/contents/ui/UserDetailsPage.qml:190
#, kde-format
msgid "Delete User…"
msgstr "删除用户…"

#: package/contents/ui/UserDetailsPage.qml:201
#, kde-format
msgid "Configure Fingerprint Authentication…"
msgstr "配置指纹身份验证…"

#: package/contents/ui/UserDetailsPage.qml:215
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Fingerprints can be used in place of a password when unlocking the screen "
"and providing administrator permissions to applications and command-line "
"programs that request them.<nl/><nl/>Logging into the system with your "
"fingerprint is not yet supported."
msgstr ""
"指纹可以用于替代密码来解除锁屏，或在应用程序和命令行程序请求管理员权限时进行"
"身份验证。<nl/><nl/>目前暂不支持使用指纹登录系统。"

#: src/fingerprintmodel.cpp:151 src/fingerprintmodel.cpp:258
#, kde-format
msgid "No fingerprint device found."
msgstr "未找到指纹设备。"

#: src/fingerprintmodel.cpp:331
#, kde-format
msgid "Retry scanning your finger."
msgstr "重新尝试扫描您的手指指纹。"

#: src/fingerprintmodel.cpp:333
#, kde-format
msgid "Swipe too short. Try again."
msgstr "滑动距离太短，请重试。"

#: src/fingerprintmodel.cpp:335
#, kde-format
msgid "Finger not centered on the reader. Try again."
msgstr "手指未在指纹读取器上居中放置。请重试。"

#: src/fingerprintmodel.cpp:337
#, kde-format
msgid "Remove your finger from the reader, and try again."
msgstr "从指纹读取器上移开手指，然后重试。"

#: src/fingerprintmodel.cpp:345
#, kde-format
msgid "Fingerprint enrollment has failed."
msgstr "指纹录入失败。"

#: src/fingerprintmodel.cpp:348
#, kde-format
msgid ""
"There is no space left for this device, delete other fingerprints to "
"continue."
msgstr "此设备没有剩余空间，请先删除其他指纹，然后继续录入。"

#: src/fingerprintmodel.cpp:351
#, kde-format
msgid "The device was disconnected."
msgstr "设备已断开连接。"

#: src/fingerprintmodel.cpp:356
#, kde-format
msgid "An unknown error has occurred."
msgstr "发生未知错误。"

#: src/user.cpp:267
#, kde-format
msgid "Could not get permission to save user %1"
msgstr "无法获取保存用户 %1 的权限"

#: src/user.cpp:272
#, kde-format
msgid "There was an error while saving changes"
msgstr "保存更改时出错"

#: src/user.cpp:368
#, kde-format
msgid "Failed to resize image: opening temp file failed"
msgstr "调整图像大小失败：无法打开临时文件"

#: src/user.cpp:376
#, kde-format
msgid "Failed to resize image: writing to temp file failed"
msgstr "调整图像大小失败：无法写入临时文件"

#: src/usermodel.cpp:138
#, kde-format
msgid "Your Account"
msgstr "您的账号"

#: src/usermodel.cpp:138
#, kde-format
msgid "Other Accounts"
msgstr "其他账号"
